'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Post = new Schema({
	autor: String,
    imagen_autor: String,
    creado: Date,
    tipo:Number,
    mensaje: String,
    compartido: Number,
    apreciado: Number,
    imagen: String,
    enlace: String
});

module.exports = mongoose.model('Post', Post);
