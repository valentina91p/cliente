angular.module("SocialHub",['ngRoute','ngCookies'])
	.config(['$routeProvider', function($routeProvider){
    	$routeProvider.when('/',{
            templateUrl: '../vistas/feed.html',
            controller: 'FeedController'
        }).when('/signin',{
        	templateUrl: '../vistas/signin.html',
            controller: 'SigninController'
        }).when('/signup',{
        	templateUrl: '../vistas/signup.html',
            controller: 'SignupController'
        }).when('/config',{
        	templateUrl: '../vistas/config.html',
            controller: 'ConfigController'
        }).when('/feed',{
        	templateUrl: '../vistas/feed.html',
            controller: 'FeedController'
        });
    }])
	.controller("FeedController",['$scope','$http','UserStore','$interval',
        function($scope,$http,UserStore,$interval){
		$scope.itemList = [];
		$scope.refrescar = function(){
            $http.post("http://localhost:3000/twitter",{token:UserStore.getUser().twitter.token,
              username:UserStore.getUser().twitter.username})
            .then(function(resp){
                console.log(resp);
                var i = 0, len = resp.data.feed.length, tweet, salir;
                for(; i < len; i++){
                    tweet = resp.data.feed[i];
                    salir = false;
                    for(var j = 0, len2 = $scope.itemList.length; j < len2; j++){
                        if(tweet.id == $scope.itemList[j].id){
                            salir = true;
                            break;
                        }
                    }
                    if(salir) break;
                    var t = {tipo: (Math.random() > 0.65)?0:1};
                        t.autor= (t.tipo==1)?tweet.user.name + " @"+tweet.user.screen_name:tweet.user.name;
                        t.imagen_autor= tweet.user.profile_image_url;
                        t.creado= tweet.created_at;
                        t.mensaje= tweet.text;
                        t.compartido= tweet.retweet_count;
                        t.apreciado= tweet.favorite_count;
                    if(tweet.entities.urls.length > 0)
                        t.enlace =  tweet.entities.urls[0].expanded_url;
                    if(tweet.entities.media && tweet.entities.media.length > 0)
                        t.imagen = tweet.entities.media[0].media_url;
                    
                    $scope.itemList.push(t);
                }
            });
            /*$http.post("http://localhost:3000/facebook",{token:UserStore.getUser().facebook.token})
            .then(function(resp){
                console.log(resp);
                var f = new Date().getTime();
                resp.data.posts.forEach(function(post, i){
                    post.creado = f-97000*i;
                    for(var i = 0; i < $scope.itemList.length; i++){
                        
                    }
                });
                //$scope.itemList = resp.data.itemList;
            });*/
		};

		$scope.refrescar();
        $interval(function(){
            $scope.refrescar();
        }, 60000);
        //$interval.
	}])
	.controller("SigninController",['$scope','$http','$location','UserStore',
        function($scope,$http,$location,UserStore){
        $scope.credentials = {};
        $scope.login = function(credentials){
            $http.post('http://localhost:3000/login',credentials)
                .then(function(resp){
                    if(resp.data.err)
                        $scope.error_msg = resp.data.err;
                    UserStore.setUser(resp.data.user);
                    $location.url("feed");
                }, function(resp){
                    $scope.error_msg = resp.data.err;
                });
        };

	}])
	.controller("SignupController",['$scope','$http','$location',function($scope,$http,$location){
        $scope.user = {};
        $scope.signup = function(user){
            $http.post('http://localhost:3000/register',user)
                .then(function(resp){
                    if(resp.data.err)
                        $scope.error_msg = resp.data.err;
                    $location.url("signin");
                }, function(resp){
                    $scope.error_msg = resp.data.err;
                });
        };
	}])
	.controller("ConfigController",['$scope','$http','UserStore',function($scope,$http,UserStore){
        $http.post("http://localhost:3000/user",{email:UserStore.getUser().local.email})
            .then(function(resp){   
                UserStore.setUser(resp.data.user);
                $scope.user = UserStore.getUser();
            });
        $scope.logout = function(){};
	}])
    .service("UserStore", function($cookies){
        function getUser(){
            return $cookies.getObject("u");
        }
        function setUser(user){
            $cookies.putObject("u",user);
        }
       return {
            getUser: getUser,
            setUser: setUser
       } 
    });
