'use strict';

var path = require('path');
var graph = require('fbgraph');
var Twit = require('twit')
var User = require('./users');
var Post = require('./posts');	
module.exports = function (app, passport) {

	function isLoggedIn (req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		} else {
			res.send(401);
		}
	}

	app.get("/", function(req, res){
		res.sendFile("index.html", { root: path.join(__dirname, '/') });
	});
	app.post('/register',function(req,res,next){
		passport.authenticate('local-signup',function(err,user,info){
			if(err) res.status(500).json({err: err});
			if(!user)
				res.status(401).json({err: info});
			else{
				res.status(200).json({err: false, data: user});
			}
		})(req, res, next);;
	});
	app.post('/login',function(req,res,next){
		passport.authenticate('local-login',function(err, user, info){
			console.log("user:",user,"err:",err,"info:",info);
			if(err)
				return res.status(500).json({err: err});
			if(!user)
				return res.status(401).json({err: info});
			else{
				req.logIn(user, function(err){
					if (err) {
				       return res.status(500).json({err: 'Could not log in user'});
				    }
				    user.local.password = "";
				    return res.status(200).json({err: false, user: user});
				});
			}
		})(req, res, next);;
	});

	app.post('/logout',function (req, res) {
			req.logout();
			res.status(200).json("ok");
		});
	app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));
	 
	// handle the callback after facebook has authenticated the user
	app.get('/auth/facebook/callback',
	  passport.authenticate('facebook', {
	    successRedirect : '/#/config',
	    failureRedirect : '/#/config?error_fb=1'
	  })
	);
	// different scopes while logging in
	app.get('/auth/twitter',  passport.authenticate('twitter'));
	 
	// handle the callback after facebook has authenticated the user
	app.get('/auth/twitter/callback',
	  passport.authenticate('twitter', {
	    successRedirect : '/#/config',
	    failureRedirect : '/#/config?error_tw=1'
	  })
	);

	app.post('/user', function(req,res){
		console.log(req.param.email);
		User.findOne({"local.email":req.body.email},function(err,user){
			if (err) {
				res.status(500).json({err: err});
			}
			res.status(200).json({err: false, user: user});
		});
	});

	app.post('/facebook', function (req, res) {
		// graph.setAccessToken(req.body.token);
		// graph.setVersion("2.4");
		// graph.get("me/feed", function(err, resp) {
		//   if(err)
		//   	return res.status(500).json({err: err});
		//   res.status(200).json({err: false, feed: resp});
		// });
		Post.find({}, function(err, posts){
			if(err)
		   		return res.status(500).json({err: err});
		   	res.status(200).json({err: false, posts: posts});
		});
	});
	app.post('/twitter', function (req, res) {
		var twitter = new Twit({
			consumer_key:'u2OS32BJtbocz4ZMgG8t0coxI',
			consumer_secret:'3qSDFXlfBitPzJ0C3pVHmiFr5EaCbxq03gOwMKeP5gjTfRAoNY',
			access_token:req.body.token,
			access_token_secret:'EBb7QxHgslkei7zUkCgR5ugM0XvurKmnmdRXR105mdAij'
		});
		var TWEET_COUNT = 15;
		var MAX_WIDTH = 305;
		var USER_TIMELINE_URL = 'statuses/home_timeline';
		var params = {
		    screen_name: req.body.username, // the user id passed in as part of the route
		    count: TWEET_COUNT // how many tweets to return
		};
		twitter.get(USER_TIMELINE_URL, params, function (err, data, resp) {
			if(err)
				return res.status(500).json({err: err});
		  res.status(200).json({err: false, feed: data});
		    // tweets = data;

		    // var i = 0, len = tweets.length;

		    // for(i; i < len; i++) {
		    //   getOEmbed(tweets[i]);
		    // }
		  });
	});
};
