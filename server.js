'use strict';

var express = require('express');
var routes = require('./routes.js');
var mongoose = require('mongoose');
var passport = require('passport');
var session = require('express-session');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

var app = express();
require('./passport')(passport);
app.use('/img', express.static(process.cwd() + '/img'));
app.use('/js', express.static(process.cwd() + '/js'));
app.use('/css', express.static(process.cwd() + '/css'));
app.use('/vistas', express.static(process.cwd() + '/vistas'));

mongoose.connect("mongodb://localhost:27017/socialhub");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride('X-HTTP-Method-Override'));

app.use(session({
	secret: 'secretClementine',
	resave: false,
	saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

var port = process.env.PORT || 3000;
routes(app, passport);
app.listen(port,  function () {
	console.log('Node.js listening on port ' + port + '...');
});