'use strict';

//var GitHubStrategy = require('passport-github').Strategy;
var User = require('./users');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
//var configAuth = require('./auth');

module.exports = function (passport) {

	passport.serializeUser(function (user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function (id, done) {
		User.findById(id, function (err, user) {
			done(err, user);
		});
	});

	passport.use('local-signup', new LocalStrategy({
			usernameField: 'email',
			passwordField: 'password',
			passReqToCallback : true
		}, function (req, email, password, done) {
				process.nextTick(function () {
					User.findOne({ 'local.email': email }, function (err, user) {
						if (err) {
							console.log('an error ocurred');
							return done(err);
						}

						if (user) {
							console.log('there\'s already an user');
							return done(null, false,"Ya existe un usuario registrado con este correo");
						} else {
							console.log('creating user');
							var user = new User();

							user.local.email = email;
							user.local.password = password;
							user.local.name = req.param('name');

							user.save(function (err) {
								if (err) {
									return done(err);
								}
								user.local.password = ""; 
								return done(null, user);

							});
						}
					});
				});
		}));

	passport.use('local-login', new LocalStrategy({
			usernameField: 'email',
			passwordField: 'password',
			passReqToCallback : true
		}, function(req, email, password, done){
			User.findOne({'local.email':email},function(err,user){
				console.log(email, password);
				if(err)
					return done(err);
				if(!user || user.local.password != password)
					return done(null, false, 'Email o contraseña incorrecta');
				else{
					return done(null,user);
				}
			});
				
			})
	);

	passport.use('facebook', new FacebookStrategy({
	  clientID        : "133608123638160",
	  clientSecret    : "606d689ded826d491258d665887f22b3",
	  callbackURL     : "http://localhost:3000/auth/facebook/callback",
	  passReqToCallback: true
	},
	 
	  // facebook will send back the tokens and profile
	  function(req,access_token, refresh_token, profile, done) {
	    // asynchronous
	    process.nextTick(function() {
	     	console.log("facebook profile",profile);
	      // find the user in the database based on their facebook id
	      User.findOne({ 'facebook.id' : profile.id }, function(err, user) {
	 
	        // if there is an error, stop everything and return that
	        // ie an error connecting to the database
	        if (err)
	          return done(err);
	 
	          // if the user is found, then log them in
	          if (user) {
	            return done(null, user); // user found, return that user
	          } else {
	            // if there is no user found with that facebook id, create them
	            User.findOne({"local.email": req.user.local.email},function(err,user){
	            	user.facebook.id    = profile.id; // set the users facebook id                 
		            user.facebook.token = access_token; // we will save the token that facebook provides to the user                    
		            user.facebook.name  = profile.name.givenName+" "+profile.name.familyName;
		            //user.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first

		            user.save(function(err) {
		             	if (err)
		                	throw err;
		 
		              	// if successful, return the new user
		              	return done(null, user);
		            });
	            });
	         } 
	      });
	    });
	}));

	passport.use('twitter', new TwitterStrategy({
	    consumerKey     : "u2OS32BJtbocz4ZMgG8t0coxI",
	    consumerSecret  : "3qSDFXlfBitPzJ0C3pVHmiFr5EaCbxq03gOwMKeP5gjTfRAoNY",
	    callbackURL     : "http://localhost:3000/auth/twitter/callback",
	    passReqToCallback: true
	  },
	  function(req,token, tokenSecret, profile, done) {
	    // make the code asynchronous
	    // User.findOne won't fire until we have all our data back from Twitter
	    process.nextTick(function() { 
	 
	      User.findOne({ 'twitter.id' : profile.id }, 
	        function(err, user) {
	          // if there is an error, stop everything and return that
	          // ie an error connecting to the database
	          if (err)
	            return done(err);
	 
	            // if the user is found then log them in
	            if (user) {
	               return done(null, user); // user found, return that user
	            } else {
	               User.findOne({'local.email': req.user.local.email},
	               	function(err,user){
	               		// set all of the user data that we need
		               user.twitter.id          = profile.id;
		               user.twitter.token       = token;
		               user.twitter.username = profile.username;
		               user.twitter.displayName = profile.displayName;
		 
		               // save our user into the database
		               user.save(function(err) {
		                 if (err)
		                   throw err;
		                 return done(null, user);
		               });
	               	});
	               
	            }
	         });
	      });
	    })
	);
};
